-- Producto [ent1]
create table "public"."producto" (
   "oid"  int4  not null,
   "categoria"  varchar(255),
   "estado_c_e"  bool,
   "nombreusuario"  varchar(255),
   "nombre"  varchar(255),
   "descripcion"  text,
   "fecha"  date,
   "precio"  float8,
   "calidad"  varchar(255),
   "oid_usuario"  varchar(255),
  primary key ("oid")
);


-- Categoria [ent2]
create table "public"."categoria" (
   "oid"  int4  not null,
   "nombrecategoria"  varchar(255),
  primary key ("oid")
);


-- Subcategoria [ent3]
create table "public"."subcategoria" (
   "oid"  int4  not null,
   "nombresubcategoria"  varchar(255),
  primary key ("oid")
);


-- Incidencia [ent4]
create table "public"."incidencia" (
   "oid"  int4  not null,
   "nombreincidencia"  varchar(255),
   "descripcion"  text,
  primary key ("oid")
);


-- Comentario [ent5]
create table "public"."comentario" (
   "oid"  int4  not null,
   "descripcion"  text,
   "oid_comprador"  varchar(255),
   "oid_vendedor"  varchar(255),
  primary key ("oid")
);


