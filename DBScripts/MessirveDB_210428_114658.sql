-- Calidad [ent8]
create table "public"."calidad" (
   "oid"  int4  not null,
   "nombre"  varchar(255),
  primary key ("oid")
);


-- Incidencia [ent4]
alter table "public"."incidencia"  add column  "oid_producto"  varchar(255);
alter table "public"."incidencia"  add column  "email"  varchar(255);


-- Favoritos [ent7]
alter table "public"."favoritos"  add column  "nombre"  varchar(255);
alter table "public"."favoritos"  add column  "oid_usuario"  varchar(255);
alter table "public"."favoritos"  add column  "precio"  float8;
alter table "public"."favoritos"  add column  "oid_producto"  varchar(255);


